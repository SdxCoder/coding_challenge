import 'dart:developer';

void main(List<String> arguments) {

  ///
  findNthSmallest([6, 2, 9, 1, 5, 15, 1, 1, 18, 3, 5], 3);

  ///
  final int sum = sumOf([901, 492, -10223, 3791, 243, 2]);
  log("Sum = $sum");

  ///
  final int value = 912;
  final int val = multiplicativePersistenceOf(value);

  log("Multiplicative Persistance of $value = $val");
}

/// Challenge No 1
///
/// Given a List of integer elements,
/// find the position of the Nth smallest element in the List
///

int findNthSmallest(List<int> integers, int nth) {
  assert(integers.length >= nth);

  final List<int> sortedList = List.from(integers);

  late int min, max;

  /// Sorting list using
  /// selection sort.
  ///
  /// Find the minimum and replace it with
  /// the first item of the list.
  for (int i = 0; i < integers.length; i++) {
    min = sortedList[i];

    for (int j = i + 1; j < integers.length; j++) {
      max = sortedList[j];

      if (min > max) {
        min = max;
        sortedList[j] = sortedList[i];
        sortedList[i] = min;
      }
    }
  }

  final List<int> removeDuplicates = [];

  ///
  /// Remove duplicates in-case if list has
  /// repititions of integers.
  for (int i = 0; i < sortedList.length; i++) {
    if (removeDuplicates.contains(sortedList[i])) {
      continue;
    } else {
      removeDuplicates.add(sortedList[i]);
    }
  }
  //
  final nthSmallest = removeDuplicates[nth - 1];

  final indexOfnthSmallest = integers.indexOf(nthSmallest);

  log("Nth position for the list = ${integers.toString()} is ${indexOfnthSmallest.toString()}");

  return indexOfnthSmallest;
}

/// ===========================================================
/// Challenge No 2
///
/// Given a List of integers, return the sum of its elements.
/// ===========================================================
///

int sumOf(List<int> integers) {
  int sum = 0;

  //
  for (int i = 0; i < integers.length; i++) {
    final int element = integers[i];

    sum = sum + element;
  }

  return sum;
}

/// ===========================================================
/// Challenge No 3
///
/// Given an integer number, value,
/// write a function that returns its multiplicative persistence.
/// ===========================================================
int multiplicativePersistence = 1;
int multiplicativePersistenceOf(int value) {
  assert(value > 0);

  final String valueString = value.toString();
  final List<int> digits = [];

  for (int i = 0; i < valueString.length; i++) {
    final String character = valueString[i];
    final int digit = int.parse(character);

    digits.add(digit);
  }

  int selfMultiplication = 1;

  for (int i = 0; i < digits.length; i++) {
    selfMultiplication = selfMultiplication * digits[i];
  }

  /// Recursively calling function
  /// untill we reach single digit.
  if (selfMultiplication.toString().length == 1) {
    return multiplicativePersistence;
  } else {
    multiplicativePersistenceOf(selfMultiplication);
    multiplicativePersistence++;
  }

  

  return multiplicativePersistence;
}
